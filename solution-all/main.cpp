#include <upcxx/upcxx.hpp>
#include <iostream>
#include <random>
#include <algorithm>

using namespace std;

tuple<mt19937_64,uniform_real_distribution<double>> createRandomGen() {
    mt19937_64 rng;
    auto timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    seed_seq ss = { uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed >> 32)};
    rng.seed(ss);
    uniform_real_distribution<double> dist(0.0,1.0);
    return make_tuple(rng, dist);
}

double runSamples(double numSamples) {
    mt19937_64 rng;
    uniform_real_distribution<double> dist;
    tie(rng, dist) = createRandomGen();
    int64_t posSamples = 0L;
    for (int i = 0; i < numSamples; i++) {
        auto xSample = dist(rng);
        auto ySample = dist(rng);
        double radius = sqrt(xSample * xSample + ySample * ySample);
        if (radius < 1.0) {
            posSamples++;
        }
    }
    return 4 * static_cast<double>(posSamples) / static_cast<double>(numSamples);
}

double accumulateAllReduce(double res) {
    auto accRes = upcxx::allreduce(res, plus<double>()).wait();
    double totalRes = accRes / static_cast<double>(upcxx::rank_n());

    upcxx::barrier();

    if (upcxx::rank_me() == 0) {
        cout << "The accumulated result: " << totalRes << endl;
    }
    return accRes;
}

double accumulateRpc(double res) {
    static double accRes = 0.0;
    static size_t receivedRanks = 0;

    auto f = [] (double myRes) {
        accRes += myRes;
        receivedRanks++;
    };

    upcxx::rpc(0, f, res);

    if (upcxx::rank_me() == 0) {
        while(receivedRanks < upcxx::rank_n()) {
            upcxx::progress();
        }

        double totalRes = accRes / static_cast<double>(upcxx::rank_n());
        cout << "The accumulated result: " << totalRes << endl;
        return totalRes;
    } else {
        return 0.0;
    }
}

double accumulateSharedObjPush(double res) {
    upcxx::global_ptr<double> resArr = nullptr;
    if (upcxx::rank_me() == 0) {
        resArr = upcxx::new_array<double>(upcxx::rank_n());
    }
    resArr = upcxx::broadcast(resArr, 0).wait();

    if (upcxx::rank_me()) {
        upcxx::rput(res, resArr + upcxx::rank_me()).wait();
    } else {
        resArr.local()[0] = res;
    }
    upcxx::barrier();
    if (!upcxx::rank_me()) {
        auto accRes = 0.0;
        auto locArr = resArr.local();
        for (int i = 0; i < upcxx::rank_n(); i++) {
            accRes += locArr[i];
        }
        accRes /= upcxx::rank_n();
        cout << "The accumulated result: " << accRes;
        cout << " -> [ ";
        for (int i = 0; i < upcxx::rank_n(); i++) {
            cout << locArr[i] << " ";
        }
        cout << "]" << endl;
        upcxx::delete_array(resArr);
        return accRes;
    } else {
        return 0.0;
    }
}

double accumulateSharedObjPull(double res) {
    upcxx::dist_object<double> distRes(res);
    if (!upcxx::rank_me()) {
        upcxx::future<double> resFut = upcxx::make_future<double>(res);
        for (int i = 1; i < upcxx::rank_n(); i++) {
            resFut = upcxx::when_all(distRes.fetch(i), resFut).then([](double a, double b) { return a + b;});
        }
        auto totalRes = resFut.wait() / upcxx::rank_n();
        cout << "The accumulated result: " << totalRes << endl;
        upcxx::barrier();
        return totalRes;
    } else {
        upcxx::barrier();
        return 0.0;
    }
}

double accumulateAtomicPush(double res) {
    upcxx::global_ptr<uint64_t> resPtr = nullptr;
    if (!upcxx::rank_me()) {
        resPtr = upcxx::new_<uint64_t>(0);
    }
    resPtr = upcxx::broadcast(resPtr, 0).wait();
    auto resInt = static_cast<uint64_t>(res * 1000000000);
    upcxx::atomic_fetch_add(resPtr, resInt, memory_order_relaxed).wait();
    upcxx::barrier();
    if (!upcxx::rank_me()) {
        double totalRes = static_cast<double>(*resPtr.local()) / (1000000000);
        totalRes /= upcxx::rank_n();
        upcxx::delete_(resPtr);
        cout << "The accumulated result: " << totalRes << endl;
        return res;
    } else {
        return 0.0;
    }
}

int main(int argc, const char **argv) {
    upcxx::init();
    if (argc != 2) {
        cout << "Usage: " << argv[0] << " <num_samples>" << endl;
        upcxx::finalize();
        return EXIT_FAILURE;
    } 
    srand(upcxx::rank_me());
    double res = runSamples(atoi(argv[1]));
    cout << "My rank is " << upcxx::rank_me() << " of " << upcxx::rank_n() << " and my sample is " << res << endl;

    accumulateAllReduce(res);
    accumulateRpc(res);
    accumulateSharedObjPush(res);
    accumulateSharedObjPull(res);
    accumulateAtomicPush(res);
    upcxx::finalize();
}
