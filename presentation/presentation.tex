\documentclass[11pt,pdftex]{beamer}

\usepackage{beamerbasecolor}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
% optionale Packages:
\usepackage{amsmath, amssymb}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{algorithm2e}

% Einbinden des SCCS-Beamer-Styles:
\usepackage{beamertheme_tumtalk} 
\usepackage{beamercolorscheme_tum} 
\usepackage{beamerfontthemestructurebold}

\newenvironment{itemblock}[1]{\begin{block}{#1}\begin{itemize}}{\end{itemize}\end{block}}


\newcommand{\concept}[1]{\textcolor{tumorange}{\bfseries{}#1}}
\renewcommand{\textbf}[1]{\textcolor{tumgruen}{\bfseries{}#1}}
\newcommand{\minisec}[1]{\textcolor{tumblau1}{\bfseries{}#1}\smallskip}
\newcommand{\referto}[1]{\textcolor{tumgruen}{\footnotesize{}#1}}

\definecolor{darkgreen}{rgb}{0,.5,0}
\definecolor{darkblue}{rgb}{0,0,.5}
\definecolor{darkred}{rgb}{0.5,0,0}
\definecolor{lightblue}{rgb}{0,0.5,1.0}

\lstset{
   language=c++,columns=flexible,showstringspaces=false,
   basicstyle=\ttfamily,
   morekeywords={register, from, to, in, parallel, class, private, protected, public},	 % keywords
   keywordstyle=\color{darkblue}\bfseries,
   keywords=[2]{__host__,__global__,__device__,__shared__,__constant__},	% cuda modifiers
   keywordstyle=[2]\color{darkred}\bfseries,
   keywords=[3]{__syncthreads,cudaError_t,cudaGetLastError,cudaSuccess,cudaMalloc,cudaMemcpy,cudaFree,blockDim,blockIdx,threadIdx,dim3,cudaMemcpyHostToDevice,cudaMemcpyDeviceToHost},	% new cuda keywords and intrinsics
   keywordstyle=[3]\color{lightblue}\bfseries,
   commentstyle=\color{darkgreen}
}

% special information
\title{UPC++ -- Introduction \& Hands-On}
\subtitle[UPC++]{APGAS, in cool.}
\author[A.Pöppl, P. Samfaß]{Alexander Pöppl \& Philipp Samfaß}
\institute{Technical University of Munich\\Chair of Scientific Computing}

\date{April 5\textsuperscript{th} 2018}

\begin{document}

\frame{\titlepage}

%======================================================================

\begin{frame}
    \frametitle{References}

    \begin{itemize}
        \item UPC++ Programming Guide
        \item UPC++ Specification
    \end{itemize}

\end{frame}


%*********************************************************************
%\part{Some theory...}
%\frame{\partpage}
%*********************************************************************

\begin{frame}{Introduction}
	\begin{itemize}
      	\item C++ library for Asynchronous Partitioned Global Address Space
        \item SPMD model with threads having access to global address space
 	\item Remote memory accesses are explicit and asynchronous to enable overlap of communication/data movement
        \item GASNet as underlying communication layer
        \item Threads are referred to as ranks
	\end{itemize}
\end{frame}

\begin{frame}{APGAS Programming Model}
	\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{apgas.png}
	\end{figure}
\end{frame}

\begin{frame}{Getting Started: Hello World with UPC++}
	\begin{itemize}
	\item{All UPC++ programs need to be initialized with \texttt{upcxx::init()}}
	\item{\texttt{upcxx::finalize()} closes the runtime}
        \item{\texttt{upcxx::rank\_me()} returns the calling thread's unique rank}
	\item{\texttt{upcxx::rank\_n()} returns the total number of ranks}
	\item{\texttt{upcxx::barrier()} provides a synchronization barrier}
	\item{Declarations in \texttt{<upcxx/upcxx.hpp>} header}
        \item{Run with \texttt{upcxx-run -n <ranks> <exe> <args...>}}
        %%TODO: tell them to download and build upcxx and the example files
	\item{\textbf{Task 1:} Write a valid UPC++ Hello World program that outputs a thread's rank (exercise 0)!}
	\end{itemize}
\end{frame}

\begin{frame}{A More Sophisticated Example: Calculation of $\pi$ with Monte Carlo}
	\begin{itemize}
	\item Sample random points from unit circle quadrant
	\item Compute $\pi$ as \texttt{pi=4*pi\_in/p\_tot} on each rank
	\item Compute final approximation as average over local approximations
	\end{itemize}
	
	\begin{figure}
	\includegraphics[scale=0.35]{pi.png}
	\end{figure}
\end{frame}

\begin{frame}{Some More UPC++ Functions...}
	\begin{itemize}
	\item Drop namespace qualifier \texttt{upcxx} for brevity
	\item \texttt{allreduce(val, op)} is an asynchronous collective call 
	\item Asynchronous calls return future object
        \item Make blocking with wait call, e.g.: \texttt{allreduce(val, op).wait()}
	\item \textbf{Task 2:} Complete exercise 1 by accumulating the local results with allreduce!	
	\end{itemize}
\end{frame}

\begin{frame}{RPC}
	\begin{itemize}
	\item UPC++ features remote procedure calls with \texttt{rpc(recipient\_rank, function, args)}
	\item Function can be a lambda expression
	\item \texttt{rpc(...)} returns a future by default which can be waited on
	\item Use \texttt{progress()} to give the runtime chance to complete outstanding communication
 	\item \textbf{Task 3:} Complete exercise 2 by replacing the allreduce through rpc's! Think about sufficient synchronization...
	\end{itemize}
\end{frame}

\begin{frame}{Global Memory Pointers}
	\begin{itemize}
	\item Global pointer points to an object allocated in shared memory segment
	\item Allocation with \texttt{global\_ptr<T> gptr = new\_<T>(args)} or
	      \texttt{global\_ptr<T> gptr = new\_array<T>(size)} (\texttt{delete\_()} and \texttt{delete\_array()} for deallocation)
        \item Broadcast global pointer with \texttt{broadcast(gptr, src\_rank)}
	\item Write to remote memory with \texttt{rput(value, gptr).wait()} 
	\item Get local pointer on local rank with \texttt{gptr.local()}
	\item \textbf{Task 4:} Complete exercise 3 by using global memory accesses to reduce the local results on rank 0!
	\end{itemize}
\end{frame}

\begin{frame}{Atomics}
	\begin{itemize}
	\item UPC++ provides atomics on shared objects for integers
        \item \texttt{atomic\_fetch\_add(gptr, value, memory\_order\_relaxed).wait()} adds value to the integer referred to by gptr
	\item \textbf{Task 5:} Complete exercise 4 by modifying your previous implementation such that it uses a single integer to accumulate the result!  
	\end{itemize}
\end{frame}

\begin{frame}{Distributed Objects}
	\begin{itemize}
	\item A single logical object partitioned over ranks with a globally 
	unique identifier
	\item Each rank has its own value of the object
	\item \texttt{fetch(object, rank).wait()} returns value of object on
	 rank
	\item The collective operation \texttt{dist\_object<T> object(args)} constructs
	the distributed object
	\item The local value of the distributed object can be set with \texttt{*object = ...} 
	\item \textbf{Task 6:} Complete exercise 6 by using a distributed object
	 holding the local results! Use fetch to accumulate the local values!
	\end{itemize}
\end{frame}

\end{document}
