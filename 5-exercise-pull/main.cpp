#include <upcxx/upcxx.hpp>
#include <iostream>
#include <random>
#include <algorithm>

using namespace std;

tuple<mt19937_64,uniform_real_distribution<double>> createRandomGen() {
    mt19937_64 rng;
    auto timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    seed_seq ss = { uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed >> 32)};
    rng.seed(ss);
    uniform_real_distribution<double> dist(0.0,1.0);
    return make_tuple(rng, dist);
}

double runSamples(double numSamples) {
    mt19937_64 rng;
    uniform_real_distribution<double> dist;
    tie(rng, dist) = createRandomGen();
    int64_t posSamples = 0L;
    for (int i = 0; i < numSamples; i++) {
        auto xSample = dist(rng);
        auto ySample = dist(rng);
        double radius = sqrt(xSample * xSample + ySample * ySample);
        if (radius < 1.0) {
            posSamples++;
        }
    }
    return 4 * static_cast<double>(posSamples) / static_cast<double>(numSamples);
}


double accumulateSharedObjPull(double res) {
    cout << "~*# IMPLEMENTATION MISSING #*~" << endl;
    return 0.0;
}

int main(int argc, const char **argv) {
    upcxx::init();
    if (argc != 2) {
        cout << "Usage: " << argv[0] << " <num_samples>" << endl;
        upcxx::finalize();
        return EXIT_FAILURE;
    } 
    srand(upcxx::rank_me());
    double res = runSamples(atoi(argv[1]));
    cout << "My rank is " << upcxx::rank_me() << " of " << upcxx::rank_n() << " and my sample is " << res << endl;

    accumulateSharedObjPull(res);
    upcxx::finalize();
}
